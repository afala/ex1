/* my_module.h */

/*************************************************************************/

#ifndef _MY_MODULE_H_
#define _MY_MODULE_H_

/*************************************************************************/

// Includes:

#include <linux/ioctl.h>

/*************************************************************************/

// Constants:
#define MY_MAGIC 'r'

/*************************************************************************/

// Macros:

#define MY_RESET _IOW(MY_MAGIC, 0, int)
#define MY_RESTART _IOW(MY_MAGIC, 1, int)

/*************************************************************************/

// Function prototypes:

int my_open(struct inode *, struct file *);

int my_release(struct inode *, struct file *);

ssize_t my_read(struct file *, char *, size_t, loff_t *);

ssize_t my_write(struct file *, const char *, size_t, loff_t *);

int my_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);

#endif // _MY_MODULE_H_

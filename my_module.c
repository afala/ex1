/* my_module.c */

/*************************************************************************/

/* KERNEL */

// Defines:
#define MODULE
#define LINUX
#define __KERNEL__

/*************************************************************************/

// Includes:

#include <linux/kernel.h>  	
#include <linux/module.h>
#include <linux/fs.h>       		
#include <asm/uaccess.h>
#include <linux/errno.h> 
#include <linux/slab.h> 

#include "my_module.h"

/*************************************************************************/

// Constants:

#define MY_DEVICE "my_device"
#define BUFF_LEN 4096
#define MINORS_NUM 256
#define SUCCESS 0
#define FAILURE -1

/*************************************************************************/

// Macros:

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Anonymous");

/*************************************************************************/

// Structs:

typedef struct permissions_t {
    int read_permission;
    int write_permission;
} permissions_t;

typedef struct buffstate_t {
	char* p_buffer;
	int write_index;
	int read_index;
    permissions_t permissions; 
} buffstate_t;

struct file_operations my_fops = {
    .open = my_open,
    .release = my_release,
    .read = my_read,
    .write = my_write,
    .ioctl = my_ioctl
};

/*************************************************************************/

// Globals:

buffstate_t minors[MINORS_NUM];
int my_major = 0; /* will hold the major # of my device driver */

/*************************************************************************/

// Functions:

int init_module(void)
{
	
    my_major = register_chrdev(my_major, MY_DEVICE, &my_fops);
    if (my_major < 0) {
		printk(KERN_WARNING "cant get dynamic major: %d\n", my_major);
		return my_major;
    }
	int i;
    for(i=0; i<MINORS_NUM; i++)
	{ //init each minor assosiated with this major
		minors[i].p_buffer = NULL;
		minors[i].write_index = 0;
		minors[i].read_index = 0;
        minors[i].permissions.read_permission = 0;
        minors[i].permissions.write_permission = 0;
	}
    return SUCCESS;
}


void cleanup_module(void)
{
    int res = unregister_chrdev(my_major, MY_DEVICE);
	if (res < 0) {
		printk(KERN_WARNING "cant remove device from system: %d\n", res);
		return;
	}
	// Free every used minor
	int i;
	for (i=0; i<MINORS_NUM; i++)
	{
		// Check if current minor used buffer (meaning there was kmalloc)
		if (!minors[i].p_buffer)
		{
			kfree(minors[i].p_buffer);
		}
	}
	
    return;
}


int my_open(struct inode *inode, struct file *filp)
{
	int minor = MINOR(inode->i_rdev);	//extract minor
	if(minor < 0)
	{
		printk(KERN_WARNING "can't open device: %d\n", minor);
		return FAILURE;
	}
    
    // If minor isn't used, allocate buffer for it
    if(minors[minor].p_buffer == NULL)
    {
        minors[minor].p_buffer = kmalloc(BUFF_LEN, GFP_KERNEL);
        if (!minors[minor].p_buffer){
            printk(KERN_WARNING "memory allocation failed, cant open");
            return -ENOMEM;
        }
    }
    
    // Save the current state in order to use the buffer in read/write/ioctl
    filp->private_data = (void*)(&minors[minor]); 
    
    // TODO: Add permissions check
    if (filp->f_mode & FMODE_READ)
    {
		minors[minor].permissions.read_permission = 1;
    }
    if (filp->f_mode & FMODE_WRITE)
    {
        minors[minor].permissions.write_permission = 1;
    }
	
	
    return SUCCESS;
}


int my_release(struct inode *inode, struct file *filp)
{
    int minor = MINOR(inode->i_rdev);
    if (filp->f_mode & FMODE_READ)
    {
        minors[minor].permissions.read_permission = 0;
    }
    
    if (filp->f_mode & FMODE_WRITE)
    {
        minors[minor].permissions.write_permission = 0;
    }

    return 0;
}


ssize_t my_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
    //
    // Do read operation.
    // Return number of bytes read.
	
	// First, check for NULL pointers
	if ((filp == NULL) || (buf == NULL) )
	{
		return -EINVAL;
	}
	
	// Restore the buffer state from the private_data field.
	buffstate_t *p_buff_state = (buffstate_t *)filp->private_data;
    
    if (p_buff_state == NULL)
    {
        return -EINVAL;
    }
	
    // If there are no read permissions, quit
    if (!p_buff_state->permissions.read_permission)
    {
        printk(KERN_WARNING "Tried to write with no permissions!");
        return -EPERM;
    }
	
	// We can't allow reading bytes over the ammount written.
	// So we first check the read and write indices:
	int read_index = p_buff_state->read_index;
	int write_index = p_buff_state->write_index;
	// Initialize it to the most possible amount of bytes that can be read
	int num_bytes_to_read = write_index - read_index;
	// If it is more than the ammount requested, fix it to the requested amount.
	if (num_bytes_to_read > count)
	{
		num_bytes_to_read = count;
	}
	
	char *p_kernel_buff = p_buff_state->p_buffer;
	
	int ammount_failed = copy_to_user(buf, &(p_kernel_buff[read_index]), (size_t)num_bytes_to_read);
	int num_of_bytes_read = num_bytes_to_read - ammount_failed;
	
	// Advance the read index
	p_buff_state->read_index += num_of_bytes_read;
	
	return num_of_bytes_read;
}


ssize_t my_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos)
{
    //
    // Do write operation.
    // Return number of bytes written.
	
	// First, check for NULL pointers
	if ((filp == NULL) || (buf == NULL) || (f_pos == NULL))
	{
		return -EINVAL;
	}
    
    
	// Restore the buffer state from the private_data field.
	buffstate_t *p_buff_state = (buffstate_t *)filp->private_data;
    
    if (p_buff_state == NULL)
    {
        return -EINVAL;
    }
	
    // If there are no write permissions, quit
    if (!p_buff_state->permissions.write_permission)
    {
        printk(KERN_WARNING "Tried to write with no permissions!");
        return -EPERM;
    }
    
    
	int write_index = p_buff_state->write_index;
	
	// If trying to write over the buffer size, return -ENOMEM
	if (write_index + count > BUFF_LEN)
	{
		return -ENOMEM;
	}
	
	char *p_kernel_buff = p_buff_state->p_buffer;
	
	int ammount_failed = copy_from_user(&(p_kernel_buff[write_index]), buf, count);
	int num_of_bytes_written = count - ammount_failed;
	
	// Advance the write index
	p_buff_state->write_index += num_of_bytes_written;
	
	return num_of_bytes_written;
	
}


int my_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{	
	// Restore the buffer state from the private_data field.
	buffstate_t* p_buff_state = (buffstate_t*) filp->private_data;
    
	switch(cmd)
    {
		case MY_RESET:
		{		
			p_buff_state->read_index = 0;
			p_buff_state->write_index = 0;
			break;
		}
		
		case MY_RESTART:
		{
			p_buff_state->read_index = 0;
			break;
		}
		default:
		{
			return -ENOTTY;
		}
    }
    return SUCCESS;
}

#!/bin/bash
module_name="my_module"
device_name="my_device"
insmod ./$module_name.o
major_number=$(cat /proc/devices | grep $device_name)
major_number=($major_number)
echo Major:$major_number
minor_number=$1
mknod /dev/$device_name$minor_number c $major_number $minor_number